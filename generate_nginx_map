#!/usr/bin/env python

"""
Generates an nginx map file to redirect from Conifer URLs to Primo VE URLs

Input files consist of lines like:

991000855479705165,1-01ocul_lu,
991009020939705165,10-01ocul_lu,

nginx map file lives in conf.d and looks like:

map $request_uri $new_uri {
	default https://omni.laurentian.ca/discovery/search?vid=01OCUL_LU:OMNI;
	"~^/eg/opac/record/1(\?.*?)?$" https://omni.laurentian.ca/permalink/01OCUL_LU/kn8csu/alma991000855479705165;
	"~^/eg/opac/record/10(\?.*?)?$" https://omni.laurentian.ca/permalink/01OCUL_LU/kn8csu/alma991009020939705165;
}

directive to process the map file lives in server {} block and looks like:

# process the map file
if ($new_uri != "") {
	rewrite ^(.*?)$ $new_uri? permanent;
}
"""

def generate_head(mapout, host, institute, vid):
    """
    Generates the standard set of redirects for an Evergreen site

    * mapout = map file to which you are writing
    * host = hostname of your Primo VE site
    * institute = code that uniquely identifies your library in Primo VE
    * vid = code that reflects the view you want to use
    """
    mapout.write("# map directives must live in global http block\n")
    mapout.write("map $request_uri $new_uri {\n")
    mapout.write(f"    default https://{host}/discovery/search?vid={vid};\n")
    mapout.write(f"    \"~^/eg/staff.*$\" https://{host}/mng/login?institute={institute}&auth=CAS;\n")
    mapout.write(f"    \"~^/eg/opac/home.*$\" https://{host}/discovery/search?vid={vid};\n")
    mapout.write(f"    \"~^/eg/opac/myopac.*$\" https://{host}/discovery/account?vid={vid}&section=overview;\n")

def parse(mapin, mapout, host, institute, plink):
    """
    Parses the Alma list of Alma IDs to legacy bib IDs

    * mapin = file to read list from
    * mapout = map file to which you are writing
    * host = hostname of your Primo VE site
    * institute = code that uniquely identifies your library in Primo VE
    * plink = the hash in the permalink Primo VE creates for your site
    """
    with open(mapin, "r") as infile:
        for line in infile:
            try:
                alma, conifer = pattern.match(line).groups()
            except AttributeError:
                continue
            mapout.write(f"    \"~^/eg/opac/record/{conifer}(\\?.*?)?$\" https://{host}/permalink/{institute}/{plink}/alma{alma};\n")

if __name__ == '__main__':
    import re

    mapfiles = ("01OCUL_LU_001_BIB_IDs.csv", "01OCUL_LU_002_BIB_IDs.csv")
    host = "omni.laurentian.ca"
    institute = "01OCUL_LU"
    vid = "01OCUL_LU:OMNI"
    plink = "kn8csu"

    pattern = re.compile(r"(\d*),(\d*)-.*")
    with open("map_to_alma.conf", "w") as mapout:
        generate_head(mapout, host, institute, vid)
        for f in mapfiles:
            parse(f, mapout, host, institute, plink)
        mapout.write("}")

    with open("process_alma_map.conf", "w") as procout:
        procout.write("""# process the map file
# must live within a server block
if ($new_uri != "") {
	rewrite ^(.*?)$ $new_uri? permanent;
}
""")
